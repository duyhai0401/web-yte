
<?php
$logo = \App\Entities\Envent::all();
?>
<style>
/* Mở đầu đoạn CSS HOTLINE */
.phonering-alo-phone {position:fixed;visibility:hidden;background-color:transparent;width:200px;height:200px;
cursor:pointer;z-index:200000!important;right:150px;bottom:-50px;left:-50px;display:block;
-webkit-backface-visibility:hidden;
-webkit-transform:translateZ(0);
transition:visibility .5s;
}
.phonering-alo-phone.phonering-alo-show {visibility:visible}
.phonering-alo-phone.phonering-alo-static {opacity:.6}
.phonering-alo-phone.phonering-alo-hover,.phonering-alo-phone:hover {opacity:1}
.phonering-alo-ph-circle {width:160px;height:160px;top:20px;left:20px;position:absolute;
background-color:transparent;border-radius:100%;border:2px solid rgba(30,30,30,0.4);
opacity:.1;
-webkit-animation:phonering-alo-circle-anim 1.2s infinite ease-in-out;
animation:phonering-alo-circle-anim 1.2s infinite ease-in-out;
transition:all .5s;
-webkit-transform-origin:50% 50%;
-ms-transform-origin:50% 50%;
transform-origin:50% 50%
}
.phonering-alo-phone.phonering-alo-active .phonering-alo-ph-circle {
-webkit-animation:phonering-alo-circle-anim 1.1s infinite ease-in-out!important;
animation:phonering-alo-circle-anim 1.1s infinite ease-in-out!important
}
.phonering-alo-phone.phonering-alo-static .phonering-alo-ph-circle {
-webkit-animation:phonering-alo-circle-anim 2.2s infinite ease-in-out!important;
animation:phonering-alo-circle-anim 2.2s infinite ease-in-out!important
}
.phonering-alo-phone.phonering-alo-hover .phonering-alo-ph-circle,.phonering-alo-phone:hover .phonering-alo-ph-circle {
border-color:#00aff2;
opacity:.5
}
.phonering-alo-phone.phonering-alo-green.phonering-alo-hover .phonering-alo-ph-circle,.phonering-alo-phone.phonering-alo-green:hover .phonering-alo-ph-circle {
border-color:#272d6b;
opacity:.5
}
.phonering-alo-phone.phonering-alo-green .phonering-alo-ph-circle {
border-color:#00aff2;
opacity:.5
}
.phonering-alo-phone.phonering-alo-gray.phonering-alo-hover .phonering-alo-ph-circle,.phonering-alo-phone.phonering-alo-gray:hover .phonering-alo-ph-circle {
border-color:#ccc;
opacity:.5
}
.phonering-alo-phone.phonering-alo-gray .phonering-alo-ph-circle {
border-color:#75eb50;
opacity:.5
}
.phonering-alo-ph-circle-fill {width:100px;height:100px;top:50px;left:50px;position:absolute;background-color:#000;
border-radius:100%;border:2px solid transparent;
-webkit-animation:phonering-alo-circle-fill-anim 2.3s infinite ease-in-out;
animation:phonering-alo-circle-fill-anim 2.3s infinite ease-in-out;
transition:all .5s;
-webkit-transform-origin:50% 50%;
-ms-transform-origin:50% 50%;
transform-origin:50% 50%
}
.phonering-alo-phone.phonering-alo-active .phonering-alo-ph-circle-fill {
-webkit-animation:phonering-alo-circle-fill-anim 1.7s infinite ease-in-out!important;
animation:phonering-alo-circle-fill-anim 1.7s infinite ease-in-out!important
}
.phonering-alo-phone.phonering-alo-static .phonering-alo-ph-circle-fill {
-webkit-animation:phonering-alo-circle-fill-anim 2.3s infinite ease-in-out!important;
animation:phonering-alo-circle-fill-anim 2.3s infinite ease-in-out!important;
opacity:0!important
}
.phonering-alo-phone.phonering-alo-hover .phonering-alo-ph-circle-fill,.phonering-alo-phone:hover .phonering-alo-ph-circle-fill {
background-color:rgba(39,45,107,0.5);
opacity:.75!important
}
.phonering-alo-phone.phonering-alo-green.phonering-alo-hover .phonering-alo-ph-circle-fill,.phonering-alo-phone.phonering-alo-green:hover .phonering-alo-ph-circle-fill {
background-color:rgba(39,45,107,0.5);
opacity:.75!important
}
.phonering-alo-phone.phonering-alo-green .phonering-alo-ph-circle-fill {
background-color:rgba(0,175,242,0.5);
}
.phonering-alo-phone.phonering-alo-gray.phonering-alo-hover .phonering-alo-ph-circle-fill,.phonering-alo-phone.phonering-alo-gray:hover .phonering-alo-ph-circle-fill {
background-color:rgba(204,204,204,0.5);
opacity:.75!important
}
.phonering-alo-phone.phonering-alo-gray .phonering-alo-ph-circle-fill {
background-color:rgba(117,235,80,0.5);
opacity:.75!important
}
.phonering-alo-ph-img-circle {
width:60px;
height:60px;
top:70px;
left:70px;
position:absolute;
background:rgba(30,30,30,0.1) url(https://oneweb.com.vn/uploads/phone-icon.png) no-repeat center center;
border-radius:100%;
border:2px solid transparent;
-webkit-animation:phonering-alo-circle-img-anim 1s infinite ease-in-out;
animation:phonering-alo-circle-img-anim 1s infinite ease-in-out;
-webkit-transform-origin:50% 50%;
-ms-transform-origin:50% 50%;
transform-origin:50% 50%
}

.phonering-alo-phone.phonering-alo-active .phonering-alo-ph-img-circle {
-webkit-animation:phonering-alo-circle-img-anim 1s infinite ease-in-out!important;
animation:phonering-alo-circle-img-anim 1s infinite ease-in-out!important
}

.phonering-alo-phone.phonering-alo-static .phonering-alo-ph-img-circle {
-webkit-animation:phonering-alo-circle-img-anim 0 infinite ease-in-out!important;
animation:phonering-alo-circle-img-anim 0 infinite ease-in-out!important
}

.phonering-alo-phone.phonering-alo-hover .phonering-alo-ph-img-circle,.phonering-alo-phone:hover .phonering-alo-ph-img-circle {
background-color:#00aff2;
}

.phonering-alo-phone.phonering-alo-green.phonering-alo-hover .phonering-alo-ph-img-circle,.phonering-alo-phone.phonering-alo-green:hover .phonering-alo-ph-img-circle {
background-color:#272d6b;
}

.phonering-alo-phone.phonering-alo-green .phonering-alo-ph-img-circle {
background-color:#00aff2;
}

.phonering-alo-phone.phonering-alo-gray.phonering-alo-hover .phonering-alo-ph-img-circle,.phonering-alo-phone.phonering-alo-gray:hover .phonering-alo-ph-img-circle {
background-color:#ccc;
}

.phonering-alo-phone.phonering-alo-gray .phonering-alo-ph-img-circle {
background-color:#75eb50
}

@-webkit-keyframes phonering-alo-circle-anim {
0% {
-webkit-transform:rotate(0) scale(.5) skew(1deg);
-webkit-opacity:.1
}

30% {
-webkit-transform:rotate(0) scale(.7) skew(1deg);
-webkit-opacity:.5
}

100% {
-webkit-transform:rotate(0) scale(1) skew(1deg);
-webkit-opacity:.1
}
}

@-webkit-keyframes phonering-alo-circle-fill-anim {
0% {
-webkit-transform:rotate(0) scale(.7) skew(1deg);
opacity:.2
}

50% {
-webkit-transform:rotate(0) scale(1) skew(1deg);
opacity:.2
}

100% {
-webkit-transform:rotate(0) scale(.7) skew(1deg);
opacity:.2
}
}

@-webkit-keyframes phonering-alo-circle-img-anim {
0% {
-webkit-transform:rotate(0) scale(1) skew(1deg)
}

10% {
-webkit-transform:rotate(-25deg) scale(1) skew(1deg)
}

20% {
-webkit-transform:rotate(25deg) scale(1) skew(1deg)
}

30% {
-webkit-transform:rotate(-25deg) scale(1) skew(1deg)
}

40% {
-webkit-transform:rotate(25deg) scale(1) skew(1deg)
}

50% {
-webkit-transform:rotate(0) scale(1) skew(1deg)
}

100% {
-webkit-transform:rotate(0) scale(1) skew(1deg)
}
}

@-webkit-keyframes phonering-alo-circle-anim {
0% {
-webkit-transform:rotate(0) scale(.5) skew(1deg);
transform:rotate(0) scale(.5) skew(1deg);
opacity:.1
}

30% {
-webkit-transform:rotate(0) scale(.7) skew(1deg);
transform:rotate(0) scale(.7) skew(1deg);
opacity:.5
}

100% {
-webkit-transform:rotate(0) scale(1) skew(1deg);
transform:rotate(0) scale(1) skew(1deg);
opacity:.1
}
}

@keyframes phonering-alo-circle-anim {
0% {
-webkit-transform:rotate(0) scale(.5) skew(1deg);
transform:rotate(0) scale(.5) skew(1deg);
opacity:.1
}

30% {
-webkit-transform:rotate(0) scale(.7) skew(1deg);
transform:rotate(0) scale(.7) skew(1deg);
opacity:.5
}

100% {
-webkit-transform:rotate(0) scale(1) skew(1deg);
transform:rotate(0) scale(1) skew(1deg);
opacity:.1
}
}

@-webkit-keyframes phonering-alo-circle-fill-anim {
0% {
-webkit-transform:rotate(0) scale(.7) skew(1deg);
transform:rotate(0) scale(.7) skew(1deg);
opacity:.2
}

50% {
-webkit-transform:rotate(0) scale(1) skew(1deg);
transform:rotate(0) scale(1) skew(1deg);
opacity:.2
}

100% {
-webkit-transform:rotate(0) scale(.7) skew(1deg);
transform:rotate(0) scale(.7) skew(1deg);
opacity:.2
}
}

@keyframes phonering-alo-circle-fill-anim {
0% {
-webkit-transform:rotate(0) scale(.7) skew(1deg);
transform:rotate(0) scale(.7) skew(1deg);
opacity:.2
}

50% {
-webkit-transform:rotate(0) scale(1) skew(1deg);
transform:rotate(0) scale(1) skew(1deg);
opacity:.2
}

100% {
-webkit-transform:rotate(0) scale(.7) skew(1deg);
transform:rotate(0) scale(.7) skew(1deg);
opacity:.2
}
}

@-webkit-keyframes phonering-alo-circle-img-anim {
0% {
-webkit-transform:rotate(0) scale(1) skew(1deg);
transform:rotate(0) scale(1) skew(1deg)
}

10% {
-webkit-transform:rotate(-25deg) scale(1) skew(1deg);
transform:rotate(-25deg) scale(1) skew(1deg)
}

20% {
-webkit-transform:rotate(25deg) scale(1) skew(1deg);
transform:rotate(25deg) scale(1) skew(1deg)
}

30% {
-webkit-transform:rotate(-25deg) scale(1) skew(1deg);
transform:rotate(-25deg) scale(1) skew(1deg)
}

40% {
-webkit-transform:rotate(25deg) scale(1) skew(1deg);
transform:rotate(25deg) scale(1) skew(1deg)
}

50% {
-webkit-transform:rotate(0) scale(1) skew(1deg);
transform:rotate(0) scale(1) skew(1deg)
}

100% {
-webkit-transform:rotate(0) scale(1) skew(1deg);
transform:rotate(0) scale(1) skew(1deg)
}
}

@keyframes phonering-alo-circle-img-anim {
0% {
-webkit-transform:rotate(0) scale(1) skew(1deg);
transform:rotate(0) scale(1) skew(1deg)
}

10% {
-webkit-transform:rotate(-25deg) scale(1) skew(1deg);
transform:rotate(-25deg) scale(1) skew(1deg)
}

20% {
-webkit-transform:rotate(25deg) scale(1) skew(1deg);
transform:rotate(25deg) scale(1) skew(1deg)
}

30% {
-webkit-transform:rotate(-25deg) scale(1) skew(1deg);
transform:rotate(-25deg) scale(1) skew(1deg)
}

40% {
-webkit-transform:rotate(25deg) scale(1) skew(1deg);
transform:rotate(25deg) scale(1) skew(1deg)
}

50% {
-webkit-transform:rotate(0) scale(1) skew(1deg);
transform:rotate(0) scale(1) skew(1deg)
}

100% {
-webkit-transform:rotate(0) scale(1) skew(1deg);
transform:rotate(0) scale(1) skew(1deg)
}
}

/* Kết thúc đoạn CSS HOTLINE */
</style>
 <div class="partner">

        <link href="{{ asset('theme/beptu/Scripts/owl-carousel/owl.carousel.css') }}" rel="stylesheet" />
        <link href="{{ asset('theme/beptu/Scripts/owl-carousel/owl.theme.css') }}" rel="stylesheet" />
        <script src="{{ asset('theme/beptu/Scripts/owl-carousel/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('theme/beptu/app/services/moduleServices.js') }}"></script>
        <script src="{{ asset('theme/beptu/app/controllers/moduleController.js') }}"></script>
        <!--Begin-->
        <div class="partner-content owl-carousel" >
            <h3 class="title">Đối tác</h3>
            <div class="partner-block">
                @foreach($logo as $key=>$val)
                <div class="partner-item" >
                    <a href="" target="_blank" title="">
                        <img src="{{ $val->image }}" alt="" class="img-responsive" />
                    </a>
                </div> 
                @endforeach
            </div>
            <div class="controls boxprevnext">
                <a class="prev prevlogo"><i class="fa fa-angle-left"></i></a>
                <a class="next nextlogo"><i class="fa fa-angle-right"></i></a>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                var owl = $(".partner-block");
                owl.owlCarousel({
                    autoPlay: true,
                    autoPlay: 5000,
                    items: 6,
                    slideSpeed: 1000,
                    pagination: false,
                    itemsDesktop: [1200, 6],
                    itemsDesktopSmall: [980, 5],
                    itemsTablet: [767, 4],
                    itemsMobile: [480, 2]
                });
                $(".partner-content .nextlogo").click(function () {
                    owl.trigger('owl.next');
                });
                $(".partner-content .prevlogo").click(function () {
                    owl.trigger('owl.prev');
                });
            });
        </script>
        <!--End-->
             
    </div>


    <div class="footer">

        <div class="footer-content clearfix">
            <div class="container">
                <div class="row">
                    <div class="footer-box col-md-3 col-sm-12 col-xs-12">
                        <div class="item">
                            <h3>
                                <span>Giới thiệu</span>
                            </h3>
                        </div>
                        <ul>
                            <li>
                                <a href="gioi-thieu.html">
                                    Về ch&#250;ng t&#244;i
                                </a>
                            </li>
                            <li>
                                <a href="content/linh-vuc-hoat-dong.html">
                                    Lĩnh vực hoạt động
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer-box col-md-3 col-sm-12 col-xs-12">
                        <div class="item">
                            <h3>
                                <span>Trợ gi&#250;p</span>
                            </h3>
                        </div>
                        <ul>
                            <li>
                                <a href="content/huong-dan-thanh-toan.html">
                                    Hướng dẫn thanh to&#225;n
                                </a>
                            </li>
                            <li>
                                <a href="content/quy-dinh-doi-tra.html">
                                    Quy định đổi trả
                                </a>
                            </li>
                            <li>
                                <a href="content/chinh-sach-ban-hang.html">
                                    Ch&#237;nh s&#225;ch b&#225;n h&#224;ng
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer-box box-address col-md-3 col-sm-12 col-xs-12">
                        <div class="item">
                            <h3>
                                <span>Thông tin công ty</span>
                            </h3>
                            <div class="box-address-content">
                                <b>C&#212;NG TY TNHH PH&#193;T TRIỂN C&#212;NG NGHỆ RUNTIME</b>
                                <p><i class="fa fa-map-marker"></i> 5/12A Quang Trung, P.14, Q.G&#242; Vấp, Tp.HCM</p>
                                <p>
                                    <i class="fa fa-envelope"></i>
                                    <a href="mailto:info@runtime.vn">info@runtime.vn</a>
                                </p>
                                <p>
                                    <i class="fa fa-phone"></i>
                                    Phone: (08) 66 85 85 38
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="footer-box box-social col-md-3 col-sm-12 col-xs-12">
                        <div class="item">
                            <h3>
                                <span>Facebook</span>
                            </h3>
                            <div class="fb-like-box" data-href="https://www.facebook.com/runtime.vn" data-width="289"
                                 data-height="190" data-colorscheme="dark" data-show-faces="true" data-header="false"
                                 data-stream="false" data-show-border="false">
                            </div>
                            <div class="social-icon">
                                <ul>
                                    <li><a target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="https://www.facebook.com/runtime.vn" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    <li><a target="_blank"><i class="fa fa-youtube"></i></a></li>
                                    <li><a target="_blank"><i class="fa fa-twitter "></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="hotline">
<div style="margin-bottom: 100px" id="phonering-alo-phoneIcon" class="phonering-alo-phone phonering-alo-green phonering-alo-show">
<div class="phonering-alo-ph-circle"></div>
<div class="phonering-alo-ph-circle-fill"></div>
<div class="phonering-alo-ph-img-circle">
<a class="pps-btn-img" title="Liên hệ" href="tel:0904678500"> <img style="margin-top: 2px;
    margin-left: 3px;" src="{{ asset('theme/images/phone-ring-flat.png') }}" alt="Liên hệ" width="50" class="img-responsive"/> </a>
</div>
</div>
</div>