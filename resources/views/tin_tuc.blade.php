@extends('include.design')
@section('content')
    <form name="form1" method="post" action="" id="form1">
@include('include.header')

<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CA0B0334" />
</div>
        <div>
<div>
    <div id="content">
        <div class="PageContentLoadControl">
            <div class="khoi1170">
                <div class="left">
                    <div class="subdstintuc">
                        <div class="lstdstintuc">
@foreach($post as $key=>$val)
<div class='itemtintuc'>
    <div class='khungAnh'>
        <a class='khungAnhCrop' href="{{route('web.show_post',$val->id)}}" title="">
            <img alt="" class="" src="{{$val->image}}" />
        </a>
    </div>
    <div class='ttdstintuc'>
        <div class='ngoaia'>
            <h3>
                <a class='title' href="{{route('web.show_post',$val->id)}}" title="">
                    {{$val->title}}
                </a>
            </h3>
        </div>
        <div class='ngaydang_luotxem'>

        </div>
        <div class='note'>
            {{$val->des}}
        </div>
        <a class='btn_xemthem' href="{{route('web.show_post',$val->id)}}" title='Xem thêm'>
            Xem thêm
        </a>
    </div>
</div>
@endforeach
                        </div>
                        <div class="phantrang">

                        </div>
                    </div>
                </div>

                <div class="right">


<div class="csr">

    <div class="cb"></div>
</div>

<div class="subhotrotructuyen">
    <div class="tieudedanhmuc_left">Hỗ trợ trực tuyến</div>
    <div class="lstdshotro">


<div class='itemdshotro'>
                                        <div class='vitri_hotro'>Kinh doanh 1</div>
                                        <div class='titlt_hotline'>Ngân Hà: <a href='tel: 0963328358'><span class='sohotline_ht'> 0963328358</span></a>
                                            <div class='icon'>
                                                <a href='https://zalo.me/0963328358' title='Zalo'><span class='iconzalo'></span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class='itemdshotro'>
                                        <div class='vitri_hotro'>Kinh doanh 2</div>
                                        <div class='titlt_hotline'>Ngân Hà: <a href='tel: 09673636858'><span class='sohotline_ht'> 09673636858</span></a>
                                            <div class='icon'>
                                                <a href='https://zalo.me/09673636858' title='Zalo'><span class='iconzalo'></span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class='itemdshotro'>
                                        <div class='vitri_hotro'>Kinh doanh 3</div>
                                        <div class='titlt_hotline'>Ngân Hà: <a href='tel: 0973888067'><span class='sohotline_ht'> 0973888067</span></a>
                                            <div class='icon'>
                                                <a href='https://zalo.me/0973888067' title='Zalo'><span class='iconzalo'></span></a>
                                            </div>
                                        </div>
                                    </div>


    </div>
</div>



<div class="subtinnoibat">
    <div class='tieudedanhmuc_left'>Tin tức nổi bật</div><div class='lstdstinboibat'>
 @foreach($tinnoi as $key=>$value)
                                    <div class='itemtinnoibat'>
                                        <div class='khungAnh'>
                                            <a class='khungAnhCrop' href="{{route('web.show_post',$value->id)}}" title='{{$value->title}}'>
                                                <img alt="Khuyến mại cuối năm : Quà tết ý nghĩa" class="" src="{{$value->image}}" />
                                            </a>
                                        </div>
                                        <div class='ngoaia'>
                                            <h4>
                                                <a class='title' href="{{route('web.show_post',$value->id)}}" title='{{$value->title}}'>{{$value->title}}</a>
                                            </h4>
                                        </div>
                                    </div>
                                    @endforeach
</div>
</div>

                </div>
            </div>
        </div>
    </div>
@include('include.footer')

<script type="text/javascript">
    $(window)
    .load(function () {
        $(".khungAnhCrop img")
            .each(function () {
                $(this)
                    .removeClass("wide tall")
                    .addClass((this.width / this.height > $(this).parent().width() / $(this).parent().height())
                        ? "wide"
                        : "tall");
            });
    });
</script>

</div>



        </div>
    </form>
@endsection
