


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" debug="true" lang="vi">
@include('include.head')
<body>
    <form name="form1" method="post" action="" id="form1">
<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['form1'];
if (!theForm) {
    theForm = document.form1;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>

        <div>



<div>
@include('include.header')
<div id="duongdanlink">
    <div class="khoi1170">
        <ul class="duongdan_link">
            <li><a href="/" title="Trang chủ">Trang chủ</a></li>
            <li><a href="#" title='Liên hệ'>Liên hệ</a></li>
        </ul>
    </div>
</div>

    <div id="content">
        <div class="sublienhe">
            <div class="khoi1170">
                <div class="thongtinlienhe">
                    <div class="tieude_lienhe">Thông tin liên hệ</div>
                    <div>
	<div>
		<p>
			<span style="font-size: 14px;">VPGD: 107E9 - Phương Mai - Đống Đa - Hà Nội </br>
                (mặt đường phương mai)</p>
		<p>
			<span style="font-size:14px;">Tel: 0963328358 - 09673636858 -  0973888067</span></p>
	</div>
	<p>
		<span style="font-size:14px;">Nếu qu&yacute; kh&aacute;ch c&oacute; bất cứ y&ecirc;u cầu về c&aacute;c sản phẩm hoặc dịch vụ của ch&uacute;ng t&ocirc;i, qu&yacute; kh&aacute;ch vui l&ograve;ng điền đầy đủ c&aacute;c th&ocirc;ng tin theo mẫu dưới đ&acirc;y. Ch&uacute;ng t&ocirc;i sẽ phản hồi th&ocirc;ng tin lại cho qu&yacute; kh&aacute;ch sớm nhất c&oacute; thể. Xin tr&acirc;n trọng cảm ơn!</span></p>
</div>
<div>
	&nbsp;</div>
<br />

                </div>
                <div class="hotrolienhe">
                    <div class="tieude_lienhe">Hỗ trợ liên hệ</div>
                    <div class="lstdshotrolh">

<div style="width: calc((100% - 34px)/1);" class='itemhotrolh'>
    <div class='khungAnh'>
        <a class='khungAnhCrop'>
            <img style="height: 58%;
    width: 103%;" alt="CÔNG TY CỔ PHẦN THIẾT BỊ Y TẾ NGÂN HÀ" class="" src="{{asset('images/logo_nh.png')}}" />
        </a>
    </div>
    <div class='ttnguoihotro'>
        <div class='phongban_hotro'>CÔNG TY CỔ PHẦN THIẾT BỊ Y TẾ NGÂN HÀ</div>
        <div class='title_hotro'></div>
        <div class='hotline_hotro'><span class='fontmedium'>Hotline: </span>0963328358 - 09673636858 -  0973888067</div>
        <div class='email_hotro'><span class='fontmedium'>Email:thietbiytenhanha@gmail.com</span></div>
    </div>
</div>

                    </div>
                </div>
                <div class="formlh_bandomap">
                    <div class="left">
                        <div class="tieude_lienhe">Form liên hệ</div>
                        <div class="formlienhe" id="ContactUsIndex">
                            <div class="itemformlh">
                                <span>Họ tên: <span>*</span></span>
                                <input name="DisplayLoadControl$ctl00$ctl00$tbHoTen" type="text" id="DisplayLoadControl_ctl00_ctl00_tbHoTen" class="required" />
                            </div>
                            <div class="itemformlh">
                                <span>Email: <span>*</span></span>
                                <input name="DisplayLoadControl$ctl00$ctl00$tbEmail" type="text" id="DisplayLoadControl_ctl00_ctl00_tbEmail" class="required" />
                            </div>
                            <div class="itemformlh">
                                <span>Điện thoại: <span>*</span></span>
                                <input name="DisplayLoadControl$ctl00$ctl00$tbDienThoai" type="text" id="DisplayLoadControl_ctl00_ctl00_tbDienThoai" class="required" />
                            </div>
                            <div style="display: none;">
                                <select name="DisplayLoadControl$ctl00$ctl00$ddlGuiToi" id="DisplayLoadControl_ctl00_ctl00_ddlGuiToi" class="ct-sl required">
	<option value="53">CÔNG TY CỔ PHẦN THIẾT BỊ Y TẾ NGÂN HÀ</option>

</select>
                            </div>
                            <div class="itemformlh itemnoidunglh">
                                <span>Nội dung: <span>*</span></span>
                                <textarea name="DisplayLoadControl$ctl00$ctl00$tbContent" rows="2" cols="20" id="DisplayLoadControl_ctl00_ctl00_tbContent" class="required" spellcheck="false">
</textarea>
                            </div>
                            <a onclick=" return CheckContactForm() ;" id="DisplayLoadControl_ctl00_ctl00_lbtOK" class="ct-btn guilienhe" href="javascript:__doPostBack(&#39;DisplayLoadControl$ctl00$ctl00$lbtOK&#39;,&#39;&#39;)">Gửi</a>
                        </div>
                    </div>
                    <div class="right">
                        <div class="tieude_lienhe">Bản đồ đường đi</div>
                        <div class="bandomap" id="map_lh">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.714722553095!2d105.83612421440691!3d21.004069593997773!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac7999093d81%3A0x985ce919c43433b9!2zMTA3RTkgUGjGsMahbmcgTWFpLCDEkOG7kW5nIMSQYSwgSMOgIE7hu5lpLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1570089376568!5m2!1svi!2s" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('include.footer')

<script type="text/javascript">
    $(window)
    .load(function () {
        $(".khungAnhCrop img")
            .each(function () {
                $(this)
                    .removeClass("wide tall")
                    .addClass((this.width / this.height > $(this).parent().width() / $(this).parent().height())
                        ? "wide"
                        : "tall");
            });
    });
</script>

</div>

<style>
    .notfilled {
        border: 1px solid #ff0000 !important;
    }
</style>

<script type="text/javascript">
    function CheckContactForm() {
        if (CheckInputContact('#ContactUsIndex'))
            if(CheckEmail('#DisplayLoadControl_ctl00_ctl00_tbEmail','Email không hợp lệ'))
                return true;

        return false;
    }
</script>

<script type="text/javascript">
    function loadPopup() {
        alert("Gửi liên hệ thành công ! Chúng tôi sẽ liên hệ lại với bạn trong vòng 24h")
        ResetInputContact('#ContactUsIndex');
    }
</script>

        </div>
    </form>
</body>
</html>
