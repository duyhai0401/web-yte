-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th9 11, 2019 lúc 12:54 PM
-- Phiên bản máy phục vụ: 10.1.37-MariaDB
-- Phiên bản PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `yte`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `des` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `count` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `name`, `title`, `url`, `des`, `user_id`, `type`, `status`, `created_at`, `updated_at`, `count`) VALUES
(1, 'Giường bệnh nhân', 'Giường bệnh nhân', 'giuong-benh-nhan', 'Giường bệnh nhân', 2, 'product', 2, '2019-09-10 07:12:04', '2019-09-10 07:12:04', NULL),
(2, 'Giường trông bệnh nhân', 'Giường trông bệnh nhân', 'giuong-trong-benh-nhan', 'Giường trông bệnh nhân', 2, 'product', 2, '2019-09-10 07:12:18', '2019-09-10 07:12:18', NULL),
(3, 'Đệm hơi, đệm chống loét, đệm nước', 'Đệm hơi, đệm chống loét, đệm nước', 'dem-hoi-dem-chong-loet-dem-nuoc', 'Đệm hơi, đệm chống loét, đệm nước', 2, 'product', 2, '2019-09-10 07:12:28', '2019-09-10 07:12:28', NULL),
(4, 'Máy trợ thở và thiết bị trợ thở', 'Máy trợ thở và thiết bị trợ thở', 'may-tro-tho-va-thiet-bi-tro-tho', 'Máy trợ thở và thiết bị trợ thở', 2, 'product', 2, '2019-09-10 07:12:38', '2019-09-10 07:12:38', NULL),
(5, 'Máy tạo ôxy & Bình ô xy', 'Máy tạo ôxy & Bình ô xy', 'may-tao-oxy-binh-o-xy', 'Máy tạo ôxy & Bình ô xy', 2, 'product', 2, '2019-09-10 07:12:51', '2019-09-10 07:12:51', NULL),
(6, 'Máy đo nhịp tim & nồng độ ôxy trong máu (SPO2)', 'Máy đo nhịp tim & nồng độ ôxy trong máu (SPO2)', 'may-do-nhip-tim-nong-do-oxy-trong-mau-spo2', 'Máy đo nhịp tim & nồng độ ôxy trong máu (SPO2)', 2, 'product', 2, '2019-09-10 07:17:46', '2019-09-10 07:17:46', NULL),
(7, 'Máy hút dịch', 'Máy hút dịch', 'may-hut-dich', 'Máy hút dịch', 2, 'product', 2, '2019-09-10 07:18:11', '2019-09-10 07:18:11', NULL),
(8, 'Máy xông mũi họng', 'Máy xông mũi họng', 'may-xong-mui-hong', 'Máy xông mũi họng', 2, 'product', 2, '2019-09-10 07:18:23', '2019-09-10 07:18:23', NULL),
(9, 'Xe lăn, xe đẩy', 'Xe lăn, xe đẩy', 'xe-lan-xe-day', 'Xe lăn, xe đẩy', 2, 'product', 2, '2019-09-10 07:18:38', '2019-09-10 07:18:38', NULL),
(10, 'Ghế bô vệ sinh cho bệnh nhân', 'Ghế bô vệ sinh cho bệnh nhân', 'ghe-bo-ve-sinh-cho-benh-nhan', 'Ghế bô vệ sinh cho bệnh nhân', 2, 'product', 2, '2019-09-10 07:18:50', '2019-09-10 07:18:50', NULL),
(11, 'Khung tập đi cho người già', 'Khung tập đi cho người già', 'khung-tap-di-cho-nguoi-gia', 'Khung tập đi cho người già', 2, 'product', 2, '2019-09-10 07:19:05', '2019-09-10 07:19:05', NULL),
(12, 'Thiết bị phục hồi chức năng', 'Thiết bị phục hồi chức năng', 'thiet-bi-phuc-hoi-chuc-nang', 'Thiết bị phục hồi chức năng', 2, 'product', 2, '2019-09-10 07:22:27', '2019-09-10 07:22:27', NULL),
(13, 'Vật lý trị liệu', 'Vật lý trị liệu', 'vat-ly-tri-lieu', 'Vật lý trị liệu', 2, 'product', 2, '2019-09-10 07:22:41', '2019-09-10 07:22:41', NULL),
(14, 'Giường, ghế massage', 'Giường, ghế massage', 'giuong-ghe-massage', 'Giường, ghế massage', 2, 'product', 2, '2019-09-10 07:22:54', '2019-09-10 07:22:54', NULL),
(15, 'Đồ spa', 'Đồ spa', 'do-spa', 'Đồ spa', 2, 'product', 2, '2019-09-10 07:23:07', '2019-09-10 07:23:07', NULL),
(16, 'Thiết bị chăm sóc da', 'Thiết bị chăm sóc da', 'thiet-bi-cham-soc-da', 'Thiết bị chăm sóc da', 2, 'product', 2, '2019-09-10 07:23:17', '2019-09-10 07:23:17', NULL),
(17, 'Dao, Panh kéo thẩm mỹ', 'Dao, Panh kéo thẩm mỹ', 'dao-panh-keo-tham-my', 'Dao, Panh kéo thẩm mỹ', 2, 'product', 2, '2019-09-10 07:23:28', '2019-09-10 07:23:28', NULL),
(18, 'Bồn ngâm chân hồng ngoại', 'Bồn ngâm chân hồng ngoại', 'bon-ngam-chan-hong-ngoai', 'Bồn ngâm chân hồng ngoại', 2, 'product', 2, '2019-09-10 07:23:40', '2019-09-10 07:23:40', NULL),
(19, 'Máy móc cho spa', 'Máy móc cho spa', 'may-moc-cho-spa', 'Máy móc cho spa', 2, 'product', 2, '2019-09-10 07:23:54', '2019-09-10 07:23:54', NULL),
(20, 'Các loại găng tay', 'Các loại găng tay', 'cac-loai-gang-tay', 'Các loại găng tay', 2, 'product', 2, '2019-09-10 07:24:05', '2019-09-10 07:24:05', NULL),
(21, 'Các loại khẩu trang y tế', 'Các loại khẩu trang y tế', 'cac-loai-khau-trang-y-te', 'Các loại khẩu trang y tế', 2, 'product', 2, '2019-09-10 07:24:17', '2019-09-10 07:24:17', NULL),
(22, 'Quần áo y tế, phòng khám', 'Quần áo y tế, phòng khám', 'quan-ao-y-te-phong-kham', 'Quần áo y tế, phòng khám', 2, 'product', 2, '2019-09-10 07:24:30', '2019-09-10 07:24:30', NULL),
(23, 'Các loại ống nghiệm', 'Các loại ống nghiệm', 'cac-loai-ong-nghiem', 'Các loại ống nghiệm', 2, 'product', 2, '2019-09-10 07:24:40', '2019-09-10 07:24:40', NULL),
(24, 'Các loại chỉ khâu', 'Các loại chỉ khâu', 'cac-loai-chi-khau', 'Các loại chỉ khâu', 2, 'product', 2, '2019-09-10 07:24:51', '2019-09-10 07:24:51', NULL),
(25, 'Bông băng gạc', 'Bông băng gạc', 'bong-bang-gac', 'Bông băng gạc', 2, 'product', 2, '2019-09-10 07:25:17', '2019-09-10 07:25:17', NULL),
(26, 'Các dung dịch, sát khuẩn', 'Các dung dịch, sát khuẩn', 'cac-dung-dich-sat-khuan', 'Các dung dịch, sát khuẩn', 2, 'product', 2, '2019-09-10 07:25:30', '2019-09-10 07:25:30', NULL),
(27, 'Tim mạch, huyết áp', 'Tim mạch, huyết áp', 'tim-mach-huyet-ap', 'Tim mạch, huyết áp', 2, 'product', 2, '2019-09-10 07:25:44', '2019-09-10 07:25:44', NULL),
(28, 'Thiết bị y tế gia đình, bệnh viện', 'Thiết bị y tế gia đình, bệnh viện', 'thiet-bi-y-te-gia-dinh-benh-vien', 'Thiết bị y tế gia đình, bệnh viện', 2, 'product', 2, '2019-09-10 07:25:55', '2019-09-10 07:25:55', NULL),
(29, 'Máy massage, giảm béo', 'Máy massage, giảm béo', 'may-massage-giam-beo', 'Máy massage, giảm béo', 2, 'product', 2, '2019-09-10 07:26:06', '2019-09-10 07:26:06', NULL),
(30, 'Chăn điện', 'Chăn điện', 'chan-dien', 'Chăn điện', 2, 'product', 2, '2019-09-10 07:28:49', '2019-09-10 07:28:49', NULL),
(31, 'Túi sưởi, đèn sưởi, túi chườm nóng lạnh', 'Túi sưởi, đèn sưởi, túi chườm nóng lạnh', 'tui-suoi-den-suoi-tui-chuom-nong-lanh', 'Túi sưởi, đèn sưởi, túi chườm nóng lạnh', 2, 'product', 2, '2019-09-10 07:29:05', '2019-09-10 07:29:05', NULL),
(32, 'Máy trợ thính cho người già', 'Máy trợ thính cho người già', 'may-tro-thinh-cho-nguoi-gia', 'Máy trợ thính cho người già', 2, 'product', 2, '2019-09-10 07:29:16', '2019-09-10 07:29:16', NULL),
(33, 'Tai mũi họng', 'Tai mũi họng', 'tai-mui-hong', 'Tai mũi họng', 2, 'product', 2, '2019-09-10 07:29:27', '2019-09-10 07:29:27', NULL),
(34, 'Đai nẹp y tế & thể  thao', 'Đai nẹp y tế & thể  thao', 'dai-nep-y-te-the-thao', 'Đai nẹp y tế & thể  thao', 2, 'product', 2, '2019-09-10 07:29:39', '2019-09-10 07:29:39', NULL),
(35, 'Thiết bị cho mẹ và bé', 'Thiết bị cho mẹ và bé', 'thiet-bi-cho-me-va-be', 'Thiết bị cho mẹ và bé', 2, 'product', 2, '2019-09-10 07:29:51', '2019-09-10 07:29:51', NULL),
(36, 'Thiết bị phòng khám, bệnh viện', 'Thiết bị phòng khám, bệnh viện', 'thiet-bi-phong-kham-benh-vien', 'Thiết bị phòng khám, bệnh viện', 2, 'product', 2, '2019-09-10 07:30:01', '2019-09-10 07:30:01', NULL),
(37, 'Bàn mổ, đèn mổ', 'Bàn mổ, đèn mổ', 'ban-mo-den-mo', 'Bàn mổ, đèn mổ', 2, 'product', 2, '2019-09-10 07:30:11', '2019-09-10 07:30:11', NULL),
(38, 'Đèn mổ, đèn chiếu sáng', 'Đèn mổ, đèn chiếu sáng', 'den-mo-den-chieu-sang', 'Đèn mổ, đèn chiếu sáng', 2, 'product', 2, '2019-09-10 07:30:20', '2019-09-10 07:30:20', NULL),
(39, 'Máy siêu âm', 'Máy siêu âm', 'may-sieu-am', 'Máy siêu âm', 2, 'product', 2, '2019-09-10 07:30:31', '2019-09-10 07:30:31', NULL),
(40, 'Máy cắt, Máy đốt, dao mổ', 'Máy cắt, Máy đốt, dao mổ', 'may-cat-may-dot-dao-mo', 'Máy cắt, Máy đốt, dao mổ', 2, 'product', 2, '2019-09-10 07:30:42', '2019-09-10 07:30:42', NULL),
(41, 'Tủ nồi sấy, hấp', 'Tủ nồi sấy, hấp', 'tu-noi-say-hap', 'Tủ nồi sấy, hấp', 2, 'product', 2, '2019-09-10 07:30:54', '2019-09-10 07:30:54', NULL),
(42, 'Kính hiển vi, Kính lúp, Máy tập nhược thị', 'Kính hiển vi, Kính lúp, Máy tập nhược thị', 'kinh-hien-vi-kinh-lup-may-tap-nhuoc-thi', 'Kính hiển vi, Kính lúp, Máy tập nhược thị', 2, 'product', 2, '2019-09-10 07:34:01', '2019-09-10 07:34:01', NULL),
(43, 'Máy móc và thiết bị khác', 'Máy móc và thiết bị khác', 'may-moc-va-thiet-bi-khac', 'Máy móc và thiết bị khác', 2, 'product', 2, '2019-09-10 07:34:13', '2019-09-10 07:34:13', NULL),
(44, 'Thiết bị X quang & phòng thí nghiệm', 'Thiết bị X quang & phòng thí nghiệm', 'thiet-bi-x-quang-phong-thi-nghiem', 'Thiết bị X quang & phòng thí nghiệm', 2, 'product', 2, '2019-09-10 07:34:24', '2019-09-10 07:34:24', NULL),
(45, 'Mô hình thực tập y tế', 'Mô hình thực tập y tế', 'mo-hinh-thuc-tap-y-te', 'Mô hình thực tập y tế', 2, 'product', 2, '2019-09-10 07:34:38', '2019-09-10 07:34:38', NULL),
(46, 'Cho thuê xe lăn, máy tạo ôxy & bình ô xy', 'Cho thuê xe lăn, máy tạo ôxy & bình ô xy', 'cho-thue-xe-lan-may-tao-oxy-binh-o-xy', 'Cho thuê xe lăn, máy tạo ôxy & bình ô xy', 2, 'product', 2, '2019-09-10 07:34:49', '2019-09-10 07:34:49', NULL),
(47, 'Thiết bị & Sản phẩm Đông Y', 'Thiết bị & Sản phẩm Đông Y', 'thiet-bi-san-pham-dong-y', 'Thiết bị & Sản phẩm Đông Y', 2, 'product', 2, '2019-09-10 07:35:02', '2019-09-10 07:35:02', NULL),
(48, 'Các thiết bị khác', 'Các thiết bị khác', 'cac-thiet-bi-khac', 'Các thiết bị khác', 2, 'product', 2, '2019-09-10 07:36:50', '2019-09-10 07:36:50', NULL),
(49, 'Thu mua thanh lý các thiết bị y tế', 'Thu mua thanh lý các thiết bị y tế', 'thu-mua-thanh-ly-cac-thiet-bi-y-te', 'Thu mua thanh lý các thiết bị y tế', 2, 'product', 2, '2019-09-10 07:37:01', '2019-09-10 07:37:01', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `id_object` int(11) DEFAULT NULL,
  `type_object` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `configs`
--

CREATE TABLE `configs` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `courses`
--

CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `des` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review` bigint(20) UNSIGNED DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(10) UNSIGNED NOT NULL,
  `person` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_des` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `doi_tacs`
--

CREATE TABLE `doi_tacs` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `envents`
--

CREATE TABLE `envents` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `des` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `map` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start` time DEFAULT NULL,
  `end` time DEFAULT NULL,
  `author` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(10) UNSIGNED NOT NULL,
  `person` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_des` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `homes`
--

CREATE TABLE `homes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `des` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `component` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `info` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `images`
--

INSERT INTO `images` (`id`, `name`, `image`, `alt`, `product_id`, `info`, `info1`, `info2`, `created_at`, `updated_at`) VALUES
(1, 'Ngô Duy Hải', '/upload/images/images/abc/yte/dem-giuon_636788485698411970_HasThumb_Thumb.jpg', 'Ngô Duy Hải', 1, 'Ngô Duy Hải', NULL, NULL, '2019-09-11 09:49:02', '2019-09-11 09:49:02'),
(2, 'Ngô Duy Hải', '/upload/images/images/abc/yte/dem-hoi_636899846476096396_HasThumb_Thumb.jpg', 'Ngô Duy Hải', 1, 'Ngô Duy Hải', NULL, NULL, '2019-09-11 09:49:15', '2019-09-11 09:49:15'),
(3, 'Ngô Duy Hải', '/upload/images/images/abc/yte/dem-hoi_636899846476096396_HasThumb_Thumb.jpg', 'Ngô Duy Hải', 2, 'Ngô Duy Hải', NULL, NULL, '2019-09-11 09:49:55', '2019-09-11 09:49:55'),
(4, 'Ngô Duy Hải', '/upload/images/images/abc/yte/dem-hoi-c_636773526666166115_HasThumb_Thumb.jpg', 'Ngô Duy Hải', 3, 'Ngô Duy Hải', NULL, NULL, '2019-09-11 09:51:05', '2019-09-11 09:51:05'),
(5, 'Ngô Duy Hải', '/upload/images/images/abc/yte/dem-hoi-u_636833298425168704_HasThumb_Thumb.jpg', 'Ngô Duy Hải', 4, 'Ngô Duy Hải', NULL, NULL, '2019-09-11 09:51:45', '2019-09-11 09:51:45'),
(6, 'Ngô Duy Hải', '/upload/images/images/abc/yte/dem-hoi_636899846476096396_HasThumb_Thumb.jpg', 'Ngô Duy Hải', 5, 'Ngô Duy Hải', NULL, NULL, '2019-09-11 09:52:24', '2019-09-11 09:52:24'),
(7, 'Ngô Duy Hải', '/upload/images/images/abc/yte/giuong-di_636773483112214971_HasThumb_Thumb.jpg', 'Ngô Duy Hải', 6, 'Ngô Duy Hải', NULL, NULL, '2019-09-11 09:53:02', '2019-09-11 09:53:02'),
(8, 'Ngô Duy Hải', '/upload/images/images/abc/yte/Den-cuc-t_636773483737260722_HasThumb_Thumb.jpg', 'Ngô Duy Hải', 7, 'Ngô Duy Hải', NULL, NULL, '2019-09-11 09:53:43', '2019-09-11 09:53:43'),
(9, 'Ngô Duy Hải', '/upload/images/images/abc/yte/giuong-be_636785884372564643_HasThumb_Thumb.jpg', 'Ngô Duy Hải', 8, 'Ngô Duy Hải', NULL, NULL, '2019-09-11 09:54:16', '2019-09-11 09:54:16'),
(10, 'Ngô Duy Hải', '/upload/images/images/abc/yte/giuong-5-_636785867207182839_HasThumb_Thumb.jpg', 'Ngô Duy Hải', 9, 'Ngô Duy Hải', NULL, NULL, '2019-09-11 09:54:56', '2019-09-11 09:54:56'),
(11, 'Ngô Duy Hải', '/upload/images/images/abc/yte/dem-hoi-c_636773526666166115_HasThumb_Thumb.jpg', 'Ngô Duy Hải', 10, 'Ngô Duy Hải', NULL, NULL, '2019-09-11 10:14:21', '2019-09-11 10:14:21'),
(12, 'Ngô Duy Hải', '/upload/images/images/abc/yte/dem-hoi-u_636833298425168704_HasThumb_Thumb.jpg', 'Ngô Duy Hải', 11, 'Ngô Duy Hải', NULL, NULL, '2019-09-11 10:23:55', '2019-09-11 10:23:55');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `lessons`
--

CREATE TABLE `lessons` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `des` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `order` int(10) UNSIGNED NOT NULL,
  `person` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_des` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `members`
--

CREATE TABLE `members` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` longtext COLLATE utf8mb4_unicode_ci,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_04_28_140000_create_tags_table', 1),
(4, '2019_04_28_140944_create_posts_table', 1),
(5, '2019_04_28_141024_create_products_table', 1),
(6, '2019_04_28_141036_create_pages_table', 1),
(7, '2019_04_28_141049_create_categories_table', 1),
(8, '2019_04_28_141059_create_post_cates_table', 1),
(9, '2019_04_28_141111_create_post_tags_table', 1),
(10, '2019_04_28_141124_create_product_cates_table', 1),
(11, '2019_04_28_141135_create_product_tags_table', 1),
(12, '2019_04_28_141157_create_menu_items_table', 1),
(13, '2019_04_28_141206_create_menus_table', 1),
(14, '2019_04_28_141228_create_comments_table', 1),
(15, '2019_04_28_141238_create_configs_table', 1),
(16, '2019_04_28_141249_create_contacts_table', 1),
(17, '2019_04_28_165138_create_homes_table', 1),
(18, '2019_05_29_024751_create_roles_table', 1),
(19, '2019_05_29_024909_create_role_users_table', 1),
(20, '2019_06_07_165159_update_tag_category_table', 1),
(21, '2019_06_14_193240_create_students_table', 1),
(22, '2019_06_14_193501_create_teachers_table', 1),
(23, '2019_06_14_193536_create_members_table', 1),
(24, '2019_06_14_193610_create_courses_table', 1),
(25, '2019_06_14_193652_create_envents_table', 1),
(26, '2019_06_14_195405_create_lessons_table', 1),
(27, '2019_06_15_084946_create_slugs_table', 1),
(28, '2019_08_03_011912_create_images_table', 1),
(29, '2019_09_09_103449_create_sliders_table', 1),
(30, '2019_09_09_142009_create_doi_tacs_table', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `des` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `config` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_des` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `des` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(10) UNSIGNED NOT NULL,
  `status_seo` int(11) DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_des` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `post_cates`
--

CREATE TABLE `post_cates` (
  `post_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `post_tags`
--

CREATE TABLE `post_tags` (
  `post_id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `des` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_pre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_des` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `video` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `title`, `url`, `des`, `content`, `image`, `price_pre`, `price`, `deal`, `user_id`, `meta_title`, `meta_des`, `meta_keyword`, `status`, `created_at`, `updated_at`, `video`) VALUES
(1, 'Sản phẩm 1', 'san-pham-1', 'Sản phẩm 1', '<p>Chức năng của giường &nbsp;bệnh nh&acirc;n 1 tay quay:</p>\r\n\r\n<p>Giường chăm s&oacute;c bệnh nh&acirc;n nặng điều khiển bằng tay đặc biệt thuận tiện sử dụng trong c&aacute;c ph&ograve;ng hồi sức cấp cứu.</p>\r\n\r\n<p>01 tay quay n&acirc;ng đầu</p>\r\n\r\n<h2>Th&ocirc;ng số kĩ thuật của giường b&ecirc;nh nh&acirc;n &nbsp;nh&acirc;n inox đa năng gi&aacute; rẻ:&nbsp;</h2>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;To&agrave;n bộ l&agrave;m bằng inox SUS của Nhật bản, C&aacute;c nước T&acirc;y &Acirc;u, H&agrave;n quốc kh&ocirc;ng h&uacute;t từ.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;K&iacute;ch thước giường&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; D&agrave;i 1960&nbsp;&plusmn;5 mm.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rộng 900&nbsp;&plusmn;5mm.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Cao 550&nbsp;&plusmn;5 mm.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Phần lưng điều chỉnh được độ dốc bằng 1 tay quay:&nbsp; Khoảng 0&nbsp;&cedil;60o.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Đầu giường bằng ống inox&nbsp;F32mm.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C&oacute; cọc truyền dịch v&agrave; cọc m&agrave;n inox.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D&aacute;t giường bằng inox&nbsp;&nbsp;12x48.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;Bảo h&agrave;nh sản phẩm: 12 th&aacute;ng.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Xuất xứ: Việt Nam&nbsp;</p>', '/upload/images/images/abc/yte/dem-hoi-u_636833298425168704_HasThumb_Thumb.jpg', '99', '90', 'on', 2, 'Sản phẩm 1', 'Sản phẩm 1', NULL, 2, '2019-09-11 09:48:38', '2019-09-11 09:48:38', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/3wPhzZGxzVM\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(2, 'Sản phẩm 11', 'san-pham-11', 'Sản phẩm 11', '<p>Chức năng của giường &nbsp;bệnh nh&acirc;n 1 tay quay:</p>\r\n\r\n<p>Giường chăm s&oacute;c bệnh nh&acirc;n nặng điều khiển bằng tay đặc biệt thuận tiện sử dụng trong c&aacute;c ph&ograve;ng hồi sức cấp cứu.</p>\r\n\r\n<p>01 tay quay n&acirc;ng đầu</p>\r\n\r\n<h2>Th&ocirc;ng số kĩ thuật của giường b&ecirc;nh nh&acirc;n &nbsp;nh&acirc;n inox đa năng gi&aacute; rẻ:&nbsp;</h2>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;To&agrave;n bộ l&agrave;m bằng inox SUS của Nhật bản, C&aacute;c nước T&acirc;y &Acirc;u, H&agrave;n quốc kh&ocirc;ng h&uacute;t từ.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;K&iacute;ch thước giường&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; D&agrave;i 1960&nbsp;&plusmn;5 mm.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rộng 900&nbsp;&plusmn;5mm.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Cao 550&nbsp;&plusmn;5 mm.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Phần lưng điều chỉnh được độ dốc bằng 1 tay quay:&nbsp; Khoảng 0&nbsp;&cedil;60o.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Đầu giường bằng ống inox&nbsp;F32mm.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C&oacute; cọc truyền dịch v&agrave; cọc m&agrave;n inox.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D&aacute;t giường bằng inox&nbsp;&nbsp;12x48.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;Bảo h&agrave;nh sản phẩm: 12 th&aacute;ng.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Xuất xứ: Việt Nam&nbsp;</p>', '/upload/images/images/abc/yte/dem-hoi_636899846476096396_HasThumb_Thumb.jpg', '99', '90', 'on', 2, 'Sản phẩm 11', 'Sản phẩm 11', NULL, 2, '2019-09-11 09:49:43', '2019-09-11 09:49:43', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/3wPhzZGxzVM\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(3, 'Sản phẩm 111', 'san-pham-111', 'Sản phẩm 111', '<p>Chức năng của giường &nbsp;bệnh nh&acirc;n 1 tay quay:</p>\r\n\r\n<p>Giường chăm s&oacute;c bệnh nh&acirc;n nặng điều khiển bằng tay đặc biệt thuận tiện sử dụng trong c&aacute;c ph&ograve;ng hồi sức cấp cứu.</p>\r\n\r\n<p>01 tay quay n&acirc;ng đầu</p>\r\n\r\n<h2>Th&ocirc;ng số kĩ thuật của giường b&ecirc;nh nh&acirc;n &nbsp;nh&acirc;n inox đa năng gi&aacute; rẻ:&nbsp;</h2>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;To&agrave;n bộ l&agrave;m bằng inox SUS của Nhật bản, C&aacute;c nước T&acirc;y &Acirc;u, H&agrave;n quốc kh&ocirc;ng h&uacute;t từ.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;K&iacute;ch thước giường&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; D&agrave;i 1960&nbsp;&plusmn;5 mm.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rộng 900&nbsp;&plusmn;5mm.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Cao 550&nbsp;&plusmn;5 mm.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Phần lưng điều chỉnh được độ dốc bằng 1 tay quay:&nbsp; Khoảng 0&nbsp;&cedil;60o.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Đầu giường bằng ống inox&nbsp;F32mm.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C&oacute; cọc truyền dịch v&agrave; cọc m&agrave;n inox.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D&aacute;t giường bằng inox&nbsp;&nbsp;12x48.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;Bảo h&agrave;nh sản phẩm: 12 th&aacute;ng.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Xuất xứ: Việt Nam&nbsp;</p>', '/upload/images/images/abc/yte/Den-cuc-t_636773483737260722_HasThumb_Thumb.jpg', '99', '90', 'on', 2, 'Sản phẩm 111', 'Sản phẩm 111', NULL, 2, '2019-09-11 09:50:19', '2019-09-11 09:50:19', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/3wPhzZGxzVM\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(4, 'Sản phẩm 2', 'san-pham-2', 'Sản phẩm 2', '<p>Chức năng của giường &nbsp;bệnh nh&acirc;n 1 tay quay:</p>\r\n\r\n<p>Giường chăm s&oacute;c bệnh nh&acirc;n nặng điều khiển bằng tay đặc biệt thuận tiện sử dụng trong c&aacute;c ph&ograve;ng hồi sức cấp cứu.</p>\r\n\r\n<p>01 tay quay n&acirc;ng đầu</p>\r\n\r\n<h2>Th&ocirc;ng số kĩ thuật của giường b&ecirc;nh nh&acirc;n &nbsp;nh&acirc;n inox đa năng gi&aacute; rẻ:&nbsp;</h2>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;To&agrave;n bộ l&agrave;m bằng inox SUS của Nhật bản, C&aacute;c nước T&acirc;y &Acirc;u, H&agrave;n quốc kh&ocirc;ng h&uacute;t từ.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;K&iacute;ch thước giường&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; D&agrave;i 1960&nbsp;&plusmn;5 mm.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rộng 900&nbsp;&plusmn;5mm.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Cao 550&nbsp;&plusmn;5 mm.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Phần lưng điều chỉnh được độ dốc bằng 1 tay quay:&nbsp; Khoảng 0&nbsp;&cedil;60o.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Đầu giường bằng ống inox&nbsp;F32mm.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C&oacute; cọc truyền dịch v&agrave; cọc m&agrave;n inox.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D&aacute;t giường bằng inox&nbsp;&nbsp;12x48.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;Bảo h&agrave;nh sản phẩm: 12 th&aacute;ng.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Xuất xứ: Việt Nam&nbsp;</p>', '/upload/images/images/abc/yte/Den-cuc-t_636773484010336341_HasThumb_Thumb.jpg', '99', '90', 'on', 2, 'Sản phẩm 2', 'Sản phẩm 2', NULL, 2, '2019-09-11 09:51:32', '2019-09-11 09:51:32', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/3wPhzZGxzVM\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(5, 'Sản phẩm 22', 'san-pham-22', 'Sản phẩm 22', '<p>Chức năng của giường &nbsp;bệnh nh&acirc;n 1 tay quay:</p>\r\n\r\n<p>Giường chăm s&oacute;c bệnh nh&acirc;n nặng điều khiển bằng tay đặc biệt thuận tiện sử dụng trong c&aacute;c ph&ograve;ng hồi sức cấp cứu.</p>\r\n\r\n<p>01 tay quay n&acirc;ng đầu</p>\r\n\r\n<h2>Th&ocirc;ng số kĩ thuật của giường b&ecirc;nh nh&acirc;n &nbsp;nh&acirc;n inox đa năng gi&aacute; rẻ:&nbsp;</h2>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;To&agrave;n bộ l&agrave;m bằng inox SUS của Nhật bản, C&aacute;c nước T&acirc;y &Acirc;u, H&agrave;n quốc kh&ocirc;ng h&uacute;t từ.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;K&iacute;ch thước giường&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; D&agrave;i 1960&nbsp;&plusmn;5 mm.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rộng 900&nbsp;&plusmn;5mm.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Cao 550&nbsp;&plusmn;5 mm.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Phần lưng điều chỉnh được độ dốc bằng 1 tay quay:&nbsp; Khoảng 0&nbsp;&cedil;60o.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Đầu giường bằng ống inox&nbsp;F32mm.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C&oacute; cọc truyền dịch v&agrave; cọc m&agrave;n inox.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D&aacute;t giường bằng inox&nbsp;&nbsp;12x48.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;Bảo h&agrave;nh sản phẩm: 12 th&aacute;ng.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Xuất xứ: Việt Nam&nbsp;</p>', '/upload/images/images/abc/yte/Den-cuc-t_636773483737260722_HasThumb_Thumb.jpg', '99', '90', 'on', 2, 'Sản phẩm 22', 'Sản phẩm 22', NULL, 2, '2019-09-11 09:52:10', '2019-09-11 09:52:10', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/3wPhzZGxzVM\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(6, 'Sản phẩm 222', 'san-pham-222', 'Sản phẩm 222', '<p>Chức năng của giường &nbsp;bệnh nh&acirc;n 1 tay quay:</p>\r\n\r\n<p>Giường chăm s&oacute;c bệnh nh&acirc;n nặng điều khiển bằng tay đặc biệt thuận tiện sử dụng trong c&aacute;c ph&ograve;ng hồi sức cấp cứu.</p>\r\n\r\n<p>01 tay quay n&acirc;ng đầu</p>\r\n\r\n<h2>Th&ocirc;ng số kĩ thuật của giường b&ecirc;nh nh&acirc;n &nbsp;nh&acirc;n inox đa năng gi&aacute; rẻ:&nbsp;</h2>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;To&agrave;n bộ l&agrave;m bằng inox SUS của Nhật bản, C&aacute;c nước T&acirc;y &Acirc;u, H&agrave;n quốc kh&ocirc;ng h&uacute;t từ.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;K&iacute;ch thước giường&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; D&agrave;i 1960&nbsp;&plusmn;5 mm.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rộng 900&nbsp;&plusmn;5mm.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Cao 550&nbsp;&plusmn;5 mm.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Phần lưng điều chỉnh được độ dốc bằng 1 tay quay:&nbsp; Khoảng 0&nbsp;&cedil;60o.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Đầu giường bằng ống inox&nbsp;F32mm.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C&oacute; cọc truyền dịch v&agrave; cọc m&agrave;n inox.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D&aacute;t giường bằng inox&nbsp;&nbsp;12x48.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;Bảo h&agrave;nh sản phẩm: 12 th&aacute;ng.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Xuất xứ: Việt Nam&nbsp;</p>', '/upload/images/images/abc/yte/giuong-be_636785884372564643_HasThumb_Thumb.jpg', '99', '90', 'on', 2, 'Sản phẩm 222', 'Sản phẩm 222', NULL, 2, '2019-09-11 09:52:47', '2019-09-11 09:52:47', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/3wPhzZGxzVM\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(7, 'Sản phẩm 2222', 'san-pham-2222', 'Sản phẩm 2222', '<p>Chức năng của giường &nbsp;bệnh nh&acirc;n 1 tay quay:</p>\r\n\r\n<p>Giường chăm s&oacute;c bệnh nh&acirc;n nặng điều khiển bằng tay đặc biệt thuận tiện sử dụng trong c&aacute;c ph&ograve;ng hồi sức cấp cứu.</p>\r\n\r\n<p>01 tay quay n&acirc;ng đầu</p>\r\n\r\n<h2>Th&ocirc;ng số kĩ thuật của giường b&ecirc;nh nh&acirc;n &nbsp;nh&acirc;n inox đa năng gi&aacute; rẻ:&nbsp;</h2>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;To&agrave;n bộ l&agrave;m bằng inox SUS của Nhật bản, C&aacute;c nước T&acirc;y &Acirc;u, H&agrave;n quốc kh&ocirc;ng h&uacute;t từ.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;K&iacute;ch thước giường&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; D&agrave;i 1960&nbsp;&plusmn;5 mm.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rộng 900&nbsp;&plusmn;5mm.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Cao 550&nbsp;&plusmn;5 mm.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Phần lưng điều chỉnh được độ dốc bằng 1 tay quay:&nbsp; Khoảng 0&nbsp;&cedil;60o.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Đầu giường bằng ống inox&nbsp;F32mm.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C&oacute; cọc truyền dịch v&agrave; cọc m&agrave;n inox.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D&aacute;t giường bằng inox&nbsp;&nbsp;12x48.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;Bảo h&agrave;nh sản phẩm: 12 th&aacute;ng.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Xuất xứ: Việt Nam&nbsp;</p>', '/upload/images/images/abc/yte/giuong-di_636773483112214971_HasThumb_Thumb.jpg', '99', '90', 'on', 2, 'Sản phẩm 2222', 'Sản phẩm 2222', NULL, 2, '2019-09-11 09:53:28', '2019-09-11 09:53:28', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/3wPhzZGxzVM\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(8, 'Sản phẩm 22222', 'san-pham-22222', 'Sản phẩm 22222', '<p>Chức năng của giường &nbsp;bệnh nh&acirc;n 1 tay quay:</p>\r\n\r\n<p>Giường chăm s&oacute;c bệnh nh&acirc;n nặng điều khiển bằng tay đặc biệt thuận tiện sử dụng trong c&aacute;c ph&ograve;ng hồi sức cấp cứu.</p>\r\n\r\n<p>01 tay quay n&acirc;ng đầu</p>\r\n\r\n<h2>Th&ocirc;ng số kĩ thuật của giường b&ecirc;nh nh&acirc;n &nbsp;nh&acirc;n inox đa năng gi&aacute; rẻ:&nbsp;</h2>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;To&agrave;n bộ l&agrave;m bằng inox SUS của Nhật bản, C&aacute;c nước T&acirc;y &Acirc;u, H&agrave;n quốc kh&ocirc;ng h&uacute;t từ.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;K&iacute;ch thước giường&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; D&agrave;i 1960&nbsp;&plusmn;5 mm.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rộng 900&nbsp;&plusmn;5mm.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Cao 550&nbsp;&plusmn;5 mm.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Phần lưng điều chỉnh được độ dốc bằng 1 tay quay:&nbsp; Khoảng 0&nbsp;&cedil;60o.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Đầu giường bằng ống inox&nbsp;F32mm.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C&oacute; cọc truyền dịch v&agrave; cọc m&agrave;n inox.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D&aacute;t giường bằng inox&nbsp;&nbsp;12x48.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;Bảo h&agrave;nh sản phẩm: 12 th&aacute;ng.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Xuất xứ: Việt Nam&nbsp;</p>', '/upload/images/images/abc/yte/giuong-5-_636785867207182839_HasThumb_Thumb.jpg', '99', '90', 'on', 2, 'Sản phẩm 22222', 'Sản phẩm 22222', NULL, 2, '2019-09-11 09:54:03', '2019-09-11 09:54:03', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/3wPhzZGxzVM\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(9, 'Sản phẩm 3', 'san-pham-3', 'Sản phẩm 3', '<p>Chức năng của giường &nbsp;bệnh nh&acirc;n 1 tay quay:</p>\r\n\r\n<p>Giường chăm s&oacute;c bệnh nh&acirc;n nặng điều khiển bằng tay đặc biệt thuận tiện sử dụng trong c&aacute;c ph&ograve;ng hồi sức cấp cứu.</p>\r\n\r\n<p>01 tay quay n&acirc;ng đầu</p>\r\n\r\n<h2>Th&ocirc;ng số kĩ thuật của giường b&ecirc;nh nh&acirc;n &nbsp;nh&acirc;n inox đa năng gi&aacute; rẻ:&nbsp;</h2>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;To&agrave;n bộ l&agrave;m bằng inox SUS của Nhật bản, C&aacute;c nước T&acirc;y &Acirc;u, H&agrave;n quốc kh&ocirc;ng h&uacute;t từ.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;K&iacute;ch thước giường&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; D&agrave;i 1960&nbsp;&plusmn;5 mm.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rộng 900&nbsp;&plusmn;5mm.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Cao 550&nbsp;&plusmn;5 mm.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Phần lưng điều chỉnh được độ dốc bằng 1 tay quay:&nbsp; Khoảng 0&nbsp;&cedil;60o.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Đầu giường bằng ống inox&nbsp;F32mm.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C&oacute; cọc truyền dịch v&agrave; cọc m&agrave;n inox.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D&aacute;t giường bằng inox&nbsp;&nbsp;12x48.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;Bảo h&agrave;nh sản phẩm: 12 th&aacute;ng.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Xuất xứ: Việt Nam&nbsp;</p>', '/upload/images/images/abc/yte/dem-hoi-u_636833298425168704_HasThumb_Thumb.jpg', '99', '90', 'on', 2, 'Sản phẩm 3', 'Sản phẩm 3', NULL, 2, '2019-09-11 09:54:42', '2019-09-11 09:54:42', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/3wPhzZGxzVM\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(10, 'Sản phẩm 33', 'san-pham-33', 'Sản phẩm 33', '<p>Chức năng của giường &nbsp;bệnh nh&acirc;n 1 tay quay:</p>\r\n\r\n<p>Giường chăm s&oacute;c bệnh nh&acirc;n nặng điều khiển bằng tay đặc biệt thuận tiện sử dụng trong c&aacute;c ph&ograve;ng hồi sức cấp cứu.</p>\r\n\r\n<p>01 tay quay n&acirc;ng đầu</p>\r\n\r\n<h2>Th&ocirc;ng số kĩ thuật của giường b&ecirc;nh nh&acirc;n &nbsp;nh&acirc;n inox đa năng gi&aacute; rẻ:&nbsp;</h2>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;To&agrave;n bộ l&agrave;m bằng inox SUS của Nhật bản, C&aacute;c nước T&acirc;y &Acirc;u, H&agrave;n quốc kh&ocirc;ng h&uacute;t từ.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;K&iacute;ch thước giường&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; D&agrave;i 1960&nbsp;&plusmn;5 mm.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rộng 900&nbsp;&plusmn;5mm.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Cao 550&nbsp;&plusmn;5 mm.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Phần lưng điều chỉnh được độ dốc bằng 1 tay quay:&nbsp; Khoảng 0&nbsp;&cedil;60o.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Đầu giường bằng ống inox&nbsp;F32mm.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C&oacute; cọc truyền dịch v&agrave; cọc m&agrave;n inox.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D&aacute;t giường bằng inox&nbsp;&nbsp;12x48.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;Bảo h&agrave;nh sản phẩm: 12 th&aacute;ng.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Xuất xứ: Việt Nam&nbsp;</p>', '/upload/images/images/abc/yte/Den-cuc-t_636773484010336341_HasThumb_Thumb.jpg', '99', '90', 'on', 2, 'Sản phẩm 33', 'Sản phẩm 33', NULL, 2, '2019-09-11 10:13:32', '2019-09-11 10:13:32', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/3wPhzZGxzVM\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(11, 'Sản phẩm 333', 'san-pham-333', 'Sản phẩm 333', '<p>Chức năng của giường &nbsp;bệnh nh&acirc;n 1 tay quay:</p>\r\n\r\n<p>Giường chăm s&oacute;c bệnh nh&acirc;n nặng điều khiển bằng tay đặc biệt thuận tiện sử dụng trong c&aacute;c ph&ograve;ng hồi sức cấp cứu.</p>\r\n\r\n<p>01 tay quay n&acirc;ng đầu</p>\r\n\r\n<h2>Th&ocirc;ng số kĩ thuật của giường b&ecirc;nh nh&acirc;n &nbsp;nh&acirc;n inox đa năng gi&aacute; rẻ:&nbsp;</h2>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;To&agrave;n bộ l&agrave;m bằng inox SUS của Nhật bản, C&aacute;c nước T&acirc;y &Acirc;u, H&agrave;n quốc kh&ocirc;ng h&uacute;t từ.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;K&iacute;ch thước giường&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; D&agrave;i 1960&nbsp;&plusmn;5 mm.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rộng 900&nbsp;&plusmn;5mm.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Cao 550&nbsp;&plusmn;5 mm.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Phần lưng điều chỉnh được độ dốc bằng 1 tay quay:&nbsp; Khoảng 0&nbsp;&cedil;60o.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Đầu giường bằng ống inox&nbsp;F32mm.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C&oacute; cọc truyền dịch v&agrave; cọc m&agrave;n inox.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D&aacute;t giường bằng inox&nbsp;&nbsp;12x48.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;Bảo h&agrave;nh sản phẩm: 12 th&aacute;ng.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Xuất xứ: Việt Nam&nbsp;</p>', '/upload/images/images/abc/yte/Den-cuc-t_636773484010336341_HasThumb_Thumb.jpg', '99', '90', 'on', 2, 'Sản phẩm 333', 'Sản phẩm 333', NULL, 2, '2019-09-11 10:22:02', '2019-09-11 10:22:02', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/3wPhzZGxzVM\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(12, 'Sản phẩm 3333', 'san-pham-3333', 'Sản phẩm 3333', '<p>Chức năng của giường &nbsp;bệnh nh&acirc;n 1 tay quay:</p>\r\n\r\n<p>Giường chăm s&oacute;c bệnh nh&acirc;n nặng điều khiển bằng tay đặc biệt thuận tiện sử dụng trong c&aacute;c ph&ograve;ng hồi sức cấp cứu.</p>\r\n\r\n<p>01 tay quay n&acirc;ng đầu</p>\r\n\r\n<h2>Th&ocirc;ng số kĩ thuật của giường b&ecirc;nh nh&acirc;n &nbsp;nh&acirc;n inox đa năng gi&aacute; rẻ:&nbsp;</h2>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;To&agrave;n bộ l&agrave;m bằng inox SUS của Nhật bản, C&aacute;c nước T&acirc;y &Acirc;u, H&agrave;n quốc kh&ocirc;ng h&uacute;t từ.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;K&iacute;ch thước giường&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; D&agrave;i 1960&nbsp;&plusmn;5 mm.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rộng 900&nbsp;&plusmn;5mm.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Cao 550&nbsp;&plusmn;5 mm.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Phần lưng điều chỉnh được độ dốc bằng 1 tay quay:&nbsp; Khoảng 0&nbsp;&cedil;60o.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Đầu giường bằng ống inox&nbsp;F32mm.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C&oacute; cọc truyền dịch v&agrave; cọc m&agrave;n inox.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D&aacute;t giường bằng inox&nbsp;&nbsp;12x48.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;Bảo h&agrave;nh sản phẩm: 12 th&aacute;ng.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Xuất xứ: Việt Nam&nbsp;</p>', '/upload/images/images/abc/yte/Den-cuc-t_636773484010336341_HasThumb_Thumb.jpg', '99', '90', 'on', 2, 'Sản phẩm 3333', 'Sản phẩm 3333', NULL, 2, '2019-09-11 10:28:31', '2019-09-11 10:28:31', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/3wPhzZGxzVM\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_cates`
--

CREATE TABLE `product_cates` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product_cates`
--

INSERT INTO `product_cates` (`product_id`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 2, NULL, NULL),
(3, 3, NULL, NULL),
(4, 4, NULL, NULL),
(5, 5, NULL, NULL),
(6, 6, NULL, NULL),
(7, 7, NULL, NULL),
(8, 8, NULL, NULL),
(9, 9, NULL, NULL),
(10, 10, NULL, NULL),
(11, 11, NULL, NULL),
(12, 12, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_tags`
--

CREATE TABLE `product_tags` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'Phóng viên', 'author', '{\"post.create\":true}', '2019-09-10 03:49:42', '2019-09-10 03:49:42'),
(2, 'Biên tập viên', 'editor', '{\"post.update\":true,\"post.publish\":true}', '2019-09-10 03:49:42', '2019-09-10 03:49:42'),
(3, 'admin', 'superadmin', '{\"user.view\":\"true\",\"user.create\":\"true\",\"user.update\":\"true\",\"user.delete\":\"true\",\"post.view\":\"true\",\"post.create\":\"true\",\"post.update\":\"true\",\"post.delete\":\"true\",\"page.view\":\"true\",\"page.create\":\"true\",\"page.update\":\"true\",\"page.delete\":\"true\",\"product.view\":\"true\",\"product.create\":\"true\",\"product.update\":\"true\",\"product.delete\":\"true\",\"project.view\":\"true\",\"project.create\":\"true\",\"project.update\":\"true\",\"project.delete\":\"true\",\"role.view\":\"true\",\"role.create\":\"true\",\"role.update\":\"true\",\"role.delete\":\"true\"}', '2019-09-11 09:40:00', '2019-09-11 09:40:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `role_users`
--

CREATE TABLE `role_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `role_users`
--

INSERT INTO `role_users` (`user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `slider` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `slugs`
--

CREATE TABLE `slugs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `object_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `students`
--

CREATE TABLE `students` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `des` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `count` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `teachers`
--

CREATE TABLE `teachers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `status`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Phóng viên 1', 'hoang1@gmail.com', NULL, '$2y$10$w4mMJLZFw.FzlNre8EYbWuyhVnXSL2n03rkrXHSbHHS0Y4awxBjVy', '0', NULL, NULL, '2019-09-10 03:49:42', '2019-09-10 03:49:42'),
(2, 'Phóng viên 2', 'hoang2@gmail.com', NULL, '$2y$10$k0JJ7QkuqlszQ2S1lE.Mfucmpk2.ujAdwXWvloc5TY0d3G01v1v0q', '0', NULL, NULL, '2019-09-10 03:49:42', '2019-09-10 03:49:42'),
(3, 'Biên tập viên 1', 'hoang3@gmail.com', NULL, '$2y$10$wddGHhatEInHSO2Q2JOOQ.9lg75lsSDp3hjsH6bWzx4hZcTR9/INe', '0', NULL, NULL, '2019-09-10 03:49:42', '2019-09-10 03:49:42');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `configs`
--
ALTER TABLE `configs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `configs_key_unique` (`key`);

--
-- Chỉ mục cho bảng `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `courses_url_unique` (`url`);

--
-- Chỉ mục cho bảng `doi_tacs`
--
ALTER TABLE `doi_tacs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `envents`
--
ALTER TABLE `envents`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `envents_url_unique` (`url`);

--
-- Chỉ mục cho bảng `homes`
--
ALTER TABLE `homes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `homes_name_unique` (`name`);

--
-- Chỉ mục cho bảng `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `lessons`
--
ALTER TABLE `lessons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `lessons_url_unique` (`url`),
  ADD KEY `lessons_course_id_foreign` (`course_id`);

--
-- Chỉ mục cho bảng `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `members_email_unique` (`email`);

--
-- Chỉ mục cho bảng `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Chỉ mục cho bảng `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pages_user_id_foreign` (`user_id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_url_unique` (`url`),
  ADD KEY `posts_user_id_foreign` (`user_id`);

--
-- Chỉ mục cho bảng `post_cates`
--
ALTER TABLE `post_cates`
  ADD KEY `post_cates_post_id_foreign` (`post_id`),
  ADD KEY `post_cates_category_id_foreign` (`category_id`);

--
-- Chỉ mục cho bảng `post_tags`
--
ALTER TABLE `post_tags`
  ADD KEY `post_tags_post_id_foreign` (`post_id`),
  ADD KEY `post_tags_tag_id_foreign` (`tag_id`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_user_id_foreign` (`user_id`);

--
-- Chỉ mục cho bảng `product_cates`
--
ALTER TABLE `product_cates`
  ADD KEY `product_cates_product_id_foreign` (`product_id`),
  ADD KEY `product_cates_category_id_foreign` (`category_id`);

--
-- Chỉ mục cho bảng `product_tags`
--
ALTER TABLE `product_tags`
  ADD KEY `product_tags_product_id_foreign` (`product_id`),
  ADD KEY `product_tags_tag_id_foreign` (`tag_id`);

--
-- Chỉ mục cho bảng `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Chỉ mục cho bảng `role_users`
--
ALTER TABLE `role_users`
  ADD UNIQUE KEY `role_users_user_id_role_id_unique` (`user_id`,`role_id`),
  ADD KEY `role_users_role_id_foreign` (`role_id`);

--
-- Chỉ mục cho bảng `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `slugs`
--
ALTER TABLE `slugs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slugs_slug_unique` (`slug`);

--
-- Chỉ mục cho bảng `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `students_email_unique` (`email`);

--
-- Chỉ mục cho bảng `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `teachers_email_unique` (`email`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT cho bảng `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `configs`
--
ALTER TABLE `configs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `doi_tacs`
--
ALTER TABLE `doi_tacs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `envents`
--
ALTER TABLE `envents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `homes`
--
ALTER TABLE `homes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `lessons`
--
ALTER TABLE `lessons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `members`
--
ALTER TABLE `members`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT cho bảng `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `slugs`
--
ALTER TABLE `slugs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `students`
--
ALTER TABLE `students`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `lessons`
--
ALTER TABLE `lessons`
  ADD CONSTRAINT `lessons_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `pages_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `post_cates`
--
ALTER TABLE `post_cates`
  ADD CONSTRAINT `post_cates_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `post_cates_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `post_tags`
--
ALTER TABLE `post_tags`
  ADD CONSTRAINT `post_tags_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `post_tags_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `product_cates`
--
ALTER TABLE `product_cates`
  ADD CONSTRAINT `product_cates_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_cates_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `product_tags`
--
ALTER TABLE `product_tags`
  ADD CONSTRAINT `product_tags_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_tags_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `role_users`
--
ALTER TABLE `role_users`
  ADD CONSTRAINT `role_users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `role_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
